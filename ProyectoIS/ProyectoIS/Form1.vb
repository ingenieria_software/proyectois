﻿Public Class Form1

    Private Sub Button5_Click_1(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
        PantallaPri.Close()
        Loading.Close()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Loading.Visible = True
        Me.Close()

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        TextBox1.Enabled = False
        TextBox2.Enabled = False
        TextBox3.Enabled = False
        TextBox4.Enabled = False
        ComboBox1.Enabled = False
        ComboBox2.Enabled = False
        ComboBox3.Enabled = False
        Button2.Enabled = False
        Button3.Enabled = False


        llenacombo()

        '////////////////////////////////
        TextBox5.Enabled = False
        TextBox6.Enabled = False
        TextBox7.Enabled = False
        TextBox8.Enabled = False
        ComboBox5.Enabled = False
        ComboBox6.Enabled = False
        ComboBox7.Enabled = False
        ComboBox8.Enabled = False


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        conectarse()

        TextBox1.Enabled = True
        TextBox2.Enabled = True
        TextBox3.Enabled = True
        TextBox4.Enabled = True
        ComboBox1.Enabled = True
        ComboBox2.Enabled = True
        ComboBox3.Enabled = True
        ComboBox4.Enabled = True
        Button2.Enabled = True
        Button3.Enabled = True
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        TextBox1.Enabled = False
        TextBox2.Enabled = False
        TextBox3.Enabled = False
        TextBox4.Enabled = False
        ComboBox1.Enabled = False
        ComboBox2.Enabled = False
        ComboBox3.Enabled = False
        ComboBox4.Enabled = False
        Button2.Enabled = False
        Button3.Enabled = False

        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        ComboBox1.SelectedValue = ""
        ComboBox2.SelectedValue = ""
        ComboBox3.SelectedValue = ""
        Desconectarse()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim id As Integer
        Dim nom As String = ""
        Dim ape As String = ""
        Dim tel As String = ""
        Dim habi As Integer
        Dim tipe As String = ""
        Dim precio As Integer
        Dim estado As String = ""

        If (Me.TextBox1.Text = "" And TextBox2.Text = "") Then
            MsgBox("El campo no puede ser vacio!!")


        Else

            id = Val(TextBox1.Text)
            nom = Me.TextBox2.Text
            ape = Me.TextBox3.Text
            tel = Me.TextBox4.Text
            habi = Val(ComboBox3.Text)
            tipe = Me.ComboBox1.Text
            precio = Val(Me.ComboBox4.Text)
            estado = Me.ComboBox2.Text

            cmd.CommandType = CommandType.Text
            cmd.Connection = conn
            cmd.CommandText = "INSERT INTO reservacion (IDcliente, NOMBRE, APELLIDOS, Telefono, Habitacion, Tipohabitacion, Precio, Estado)" + " VALUES (" & id & ", '" & nom & "', '" & ape & "', '" & tel & "', " & habi & ", '" & tipe & "', " & precio & ", '" & estado & "')"

            MsgBox("Se estan ingresando datos...", MsgBoxStyle.Information, "Informacion")
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                If ex.ToString.Contains("valores duplicados") Then
                    MsgBox("El registro ya existe!!")
                Else
                    MsgBox(ex.ToString)
                End If
            End Try



        End If

        TextBox1.Enabled = False
        TextBox2.Enabled = False
        TextBox3.Enabled = False
        TextBox4.Enabled = False
        ComboBox1.Enabled = False
        ComboBox2.Enabled = False
        ComboBox3.Enabled = False
        ComboBox4.Enabled = False

        Button2.Enabled = False
        Button3.Enabled = False

        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        ComboBox1.Text = ""
        ComboBox2.Text = ""
        ComboBox3.Text = ""
        ComboBox4.Text = ""

        Desconectarse()
        llenacombo()



    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim idd As Integer
        Dim nomm As String = ""
        Dim apee As String = ""
        Dim tell As String = ""

        Dim habii As Integer
        Dim tipee As String = ""
        Dim precioo As Integer
        Dim estadoo As String = ""

        If (TextBox5.Text = "" And TextBox6.Text = "") Then
            MsgBox("El campo Id y Nombre no pueden ser vacio!!")
        Else
            idd = Val(TextBox5.Text)
            nomm = TextBox6.Text
            apee = TextBox7.Text
            tell = TextBox8.Text

            habii = Val(ComboBox5.Text)
            tipee = ComboBox6.Text
            precioo = Val(ComboBox7.Text)
            estadoo = ComboBox8.Text

            cmd.CommandType = CommandType.Text
            cmd.Connection = conn

            cmd.CommandText = "UPDATE reservacion SET " + "Nombre = '" & nomm & "'," + "Apellidos = '" & apee & "', " + "Telefono = '" & tell & "', " + "Habitacion = " & habii & "," + "Tipohabitacion = '" & tipee & "'," + "Precio = " & precioo & "," + "Estado = '" & estadoo & "' " + "WHERE IDcliente = " & idd & " "
            'UPDATE reservacion SET Nombre = 'oscar' ,Telefono = '44' , Habitacion = 'doble',Tipohabitacion = 'doble',Precio = 500,Estado ='coupada' WHERE IDPERSONA =1234 
            Try
                cmd.ExecuteNonQuery()
                MsgBox("Se estan Sactualizando datos...", MsgBoxStyle.Information, "Informacion")
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

            TextBox5.Enabled = False
            TextBox6.Enabled = False
            TextBox7.Enabled = False
            TextBox8.Enabled = False
            ComboBox5.Enabled = False
            ComboBox6.Enabled = False
            ComboBox7.Enabled = False
            ComboBox8.Enabled = False

            TextBox5.Text = ""
            TextBox6.Text = ""
            TextBox7.Text = ""
            TextBox8.Text = ""
            ComboBox5.Text = ""
            ComboBox6.Text = ""
            ComboBox7.Text = ""
            ComboBox8.Text = ""

            Desconectarse()

        End If

    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        conectarse()

        TextBox5.Enabled = True
        TextBox6.Enabled = True
        TextBox7.Enabled = True
        TextBox8.Enabled = True
        ComboBox5.Enabled = True
        ComboBox6.Enabled = True
        ComboBox7.Enabled = True
        ComboBox8.Enabled = True
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        ComboBox5.Text = ""
        ComboBox6.Text = ""
        ComboBox7.Text = ""
        ComboBox8.Text = ""

        TextBox5.Enabled = False
        TextBox6.Enabled = False
        TextBox7.Enabled = False
        TextBox8.Enabled = False
        ComboBox5.Enabled = False
        ComboBox6.Enabled = False
        ComboBox7.Enabled = False
        ComboBox8.Enabled = False
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        conectarse()
        Dim ide5 As Integer
        ide5 = Val(ComboBox9.Text)

        cmd.CommandType = CommandType.Text
        cmd.Connection = conn

        cmd.CommandText = "DELETE FROM reservacion WHERE Idcliente = " & ide5 & ";"
        Try
            cmd.ExecuteNonQuery()
            Desconectarse()

            llenacombo()
            MsgBox("Se estan Borrando datos...", MsgBoxStyle.Information, "Informacion")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try


        'para regresar a vacio
        TextBox10.Text = ""
        TextBox11.Text = ""
        TextBox12.Text = ""
        TextBox13.Text = ""
        TextBox14.Text = ""
        TextBox15.Text = ""
        TextBox16.Text = ""
        ComboBox9.Text = ""

        Desconectarse()



    End Sub


    Public Sub llenacombo()
        conectarse()

        Me.ComboBox9.Items.Clear()

        cmd.CommandType = CommandType.Text
        cmd.Connection = conn

        cmd.CommandText = "SELECT Idcliente FROM reservacion"

        dr = cmd.ExecuteReader

        If dr.HasRows = True Then
            While dr.Read()
                Me.ComboBox9.Items.Add(dr.GetValue(0))
            End While
        End If
        dr.Close()
        Desconectarse()

    End Sub

    Private Sub ComboBox9_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBox9.SelectedValueChanged

        conectarse()

        If Me.ComboBox9.SelectedItem.ToString <> "" Then
            consultar(Me.ComboBox9.SelectedItem)

            If dr.Read Then
                Me.TextBox10.Text = dr(1).ToString
                Me.TextBox11.Text = dr(2).ToString
                Me.TextBox12.Text = dr(3).ToString
                Me.TextBox13.Text = dr(4).ToString
                Me.TextBox14.Text = dr(5).ToString
                Me.TextBox15.Text = dr(6).ToString
                Me.TextBox16.Text = dr(7).ToString

            Else
                MsgBox("No se encontro el registro en la base de datos")

            End If
        Else
        End If
        dr.Close()
        Desconectarse()
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        conectarse()
        ComboBox9.Enabled = True
        Desconectarse()

    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        ComboBox9.Enabled = False
        'para regresar a vacio
        TextBox10.Text = ""
        TextBox11.Text = ""
        TextBox12.Text = ""
        TextBox13.Text = ""
        TextBox14.Text = ""
        TextBox15.Text = ""
        TextBox16.Text = ""
        ComboBox9.Text = ""
        Desconectarse()

    End Sub





    Private Sub ComboBox9_TextChanged(sender As Object, e As EventArgs) Handles ComboBox9.TextChanged
        Label40.Text = ComboBox9.Text
    End Sub

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox1.KeyPress
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox2.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox3.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox4_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox4.KeyPress
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox5_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox5.KeyPress
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox8_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox8.KeyPress
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox6_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox6.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox7_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox7.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub



    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        conectarse()
        llenargrid()
        Desconectarse()

    End Sub

    Private Sub llenargrid()
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim strsql As String = "Select * from reservacion"

        Dim adp As New OleDb.OleDbDataAdapter(strsql, conn)

        ds.Tables.Add("Tabla")
        adp.Fill(ds.Tables("Tabla"))
        Me.DataGridView1.DataSource = ds.Tables("tabla")

    End Sub

    Private Sub ComboBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBox3.KeyPress
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub ComboBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBox1.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub ComboBox4_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBox4.KeyPress
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub ComboBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBox2.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub



    Private Sub ComboBox7_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBox7.KeyPress
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub ComboBox6_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBox6.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub ComboBox8_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBox8.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub ComboBox5_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBox5.KeyPress
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub

    Private Sub ComboBox9_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBox9.KeyPress
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False

        Else
            e.Handled = True
        End If
    End Sub
End Class
